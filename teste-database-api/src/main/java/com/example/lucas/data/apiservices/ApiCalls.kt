package com.example.lucas.data.apiservices

import com.example.lucas.data.repository.entity.Cell
import com.example.lucas.data.repository.entity.Fund
import io.reactivex.Single
import retrofit2.http.GET

interface ApiCalls {

    companion object {
        val BASE_URL = "https://floating-mountain-50292.herokuapp.com/"
    }

    @GET("cells.json")
    fun getCells(): Single<Cell>

    @GET("fund.json")
    fun getFund(): Single<Fund>


}