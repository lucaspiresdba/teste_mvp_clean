package com.example.lucas.data.repository.entity

import com.squareup.moshi.Json

data class DownInfoItem(


        @Json(name = "name")
        val name: String? = null,

        @Json(name = "data")
        val data: Any? = null

)