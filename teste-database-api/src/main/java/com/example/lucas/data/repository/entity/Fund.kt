package com.example.lucas.data.repository.entity

import com.squareup.moshi.Json

data class Fund(

        @Json(name = "screen")
        val screen: Screen? = null
)