package com.example.lucas.data.dagger.modules

import com.example.lucas.data.apiservices.ApiCalls
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
class WebServiceModule {

    @Singleton
    @Provides
    fun providesRetrofit(): ApiCalls {
        return Retrofit.Builder()
                .baseUrl(ApiCalls.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
                .create(ApiCalls::class.java)
    }


}