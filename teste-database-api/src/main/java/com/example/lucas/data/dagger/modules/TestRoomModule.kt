package com.example.lucas.data.dagger.modules

import android.app.Application
import androidx.room.Room
import com.example.lucas.data.repository.dao.TestRoomDao
import com.example.lucas.data.repository.database.TestRoomDataBase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class TestRoomModule {

    @Singleton
    @Provides
    fun providesDataBase(app: Application): TestRoomDataBase {
        return Room.databaseBuilder(app, TestRoomDataBase::class.java, "test-db")
                .allowMainThreadQueries().build()
    }

    @Singleton
    @Provides
    fun providesTestRoomDao(database: TestRoomDataBase): TestRoomDao {
        return database.testRoomDao()
    }
}