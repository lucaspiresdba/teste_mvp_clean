package com.example.lucas.data.repository.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "testRoom")
data class TestRoom(@PrimaryKey(autoGenerate = true) val id: Int?,
                    @ColumnInfo(name = "tipo1") val tipo1: Int,
                    @ColumnInfo(name = "tipo2") val tipo2: Int,
                    @ColumnInfo(name = "nome") val nome: String) {}


