package com.example.lucas.data.repository.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy

import androidx.room.Query
import com.example.lucas.data.repository.entity.TestRoom

@Dao
interface TestRoomDao {

    @Query("SELECT * from testRoom")
    fun getAll(): List<TestRoom>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(testRoom: TestRoom)

    @Query("DELETE from testRoom")
    fun deleteAll()
}