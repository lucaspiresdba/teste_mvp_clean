package com.example.lucas.data.repository.entity

import com.squareup.moshi.Json


data class Cell(
        @Json(name = "cells")
        val cells: List<CellsItem?>? = null
)