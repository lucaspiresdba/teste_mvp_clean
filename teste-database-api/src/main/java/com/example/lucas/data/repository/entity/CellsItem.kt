package com.example.lucas.data.repository.entity

import com.squareup.moshi.Json

data class CellsItem(

        @Json(name = "message")
        val message: String? = null,

        @Json(name = "typefield")
        val typefield: Any? = null,

        @Json(name = "hidden")
        val hidden: Boolean? = null,

        @Json(name = "show")
        val show: Any? = null,

        @Json(name = "id")
        val id: Int? = null,

        @Json(name = "type")
        val type: Int? = null,

        @Json(name = "topSpacing")
        val topSpacing: Double? = null,

        @Json(name = "required")
        val required: Boolean? = null
)