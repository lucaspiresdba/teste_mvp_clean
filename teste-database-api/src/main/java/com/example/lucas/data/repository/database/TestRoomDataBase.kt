package com.example.lucas.data.repository.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.lucas.data.repository.dao.TestRoomDao
import com.example.lucas.data.repository.entity.TestRoom

@Database(entities = [TestRoom::class], version = 1, exportSchema = false)
abstract class TestRoomDataBase : RoomDatabase() {
    abstract fun testRoomDao(): TestRoomDao
}