package com.example.lucas.tstemvpclean.dagger.modules

import com.example.lucas.tstemvpclean.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesBuilder {

    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    internal abstract fun maainActi(): MainActivity

}