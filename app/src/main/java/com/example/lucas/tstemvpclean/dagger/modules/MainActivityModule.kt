package com.example.lucas.tstemvpclean.dagger.modules

import com.example.lucas.tstemvpclean.presenter.mainActivity.MainActivityView
import com.example.lucas.tstemvpclean.ui.MainActivity
import dagger.Module
import dagger.Provides

@Module(includes = [PresentationModule::class])
class MainActivityModule {
    @Provides
    fun providesMainAct(activity: MainActivity): MainActivityView {
        return activity
    }
}