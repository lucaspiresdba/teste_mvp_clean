package com.example.lucas.tstemvpclean.dagger.modules

import com.example.lucas.domain.dataBaseUseCase.DataBaseInterface
import com.example.lucas.domain.restUseCase.InfraInterface
import com.example.lucas.tstemvpclean.presenter.mainActivity.MainActivityPresenter
import com.example.lucas.tstemvpclean.presenter.mainActivity.MainActivityView
import dagger.Module
import dagger.Provides

@Module
class PresentationModule {

    @Provides
    fun mainActPresenter(infraInterface: InfraInterface, view: MainActivityView, dataBaseInterface: DataBaseInterface): MainActivityPresenter {
        return MainActivityPresenter(infraInterface, view, dataBaseInterface)
    }
}