package com.example.lucas.tstemvpclean.dagger.modules

import android.app.Application
import android.content.Context
import com.example.lucas.tstemvpclean.app.App
import dagger.Module
import dagger.Provides

@Module
class AppModule {
    @Provides
    fun providesApp(app: App): Context {
        return app
    }
}