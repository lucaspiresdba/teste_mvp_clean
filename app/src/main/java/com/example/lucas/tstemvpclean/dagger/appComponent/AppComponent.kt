package com.example.lucas.tstemvpclean.dagger.appComponent

import android.app.Application
import com.example.lucas.data.dagger.modules.WebServiceModule
import com.example.lucas.domain.dagger.modules.DataBaseUseCaseModule
import com.example.lucas.domain.dagger.modules.InfraestructureWebModule
import com.example.lucas.tstemvpclean.app.App
import com.example.lucas.tstemvpclean.dagger.modules.ActivitiesBuilder
import com.example.lucas.tstemvpclean.dagger.modules.AppModule
import com.example.lucas.tstemvpclean.dagger.modules.MainActivityModule
import com.example.lucas.tstemvpclean.dagger.modules.PresentationModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class,
    WebServiceModule::class,
    InfraestructureWebModule::class,
    ActivitiesBuilder::class,
    PresentationModule::class,
    AppModule::class,
    MainActivityModule::class,
    DataBaseUseCaseModule::class])

interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }

    abstract fun inject(target: App)
}