package com.example.lucas.tstemvpclean.presenter.mainActivity

import com.example.lucas.data.repository.entity.TestRoom
import com.example.lucas.domain.dataBaseUseCase.DataBaseInterface
import com.example.lucas.domain.restUseCase.InfraInterface
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class MainActivityPresenter(private var infra: InfraInterface, private var view: MainActivityView, private var database: DataBaseInterface) {

    fun getCell() {
        infra.getCellsInfra()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ view.showItemsCells(it) },
                        {
                            it.printStackTrace()
                            view.error()
                        })
    }

    fun getFund() {
        infra.getFundInfra()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ view.showItemsDownInfo(it) },
                        {
                            it.printStackTrace()
                            view.error()
                        })
    }

    fun insertTest() {
        database.insertTest(TestRoom(null, (0..100).shuffled().last(), (0..100).shuffled().last(), Date().toString()))
        view.updateRowSize(getRowSize())
        view.updateRecycle(getAllTests())
    }

    fun deleteAllTests() {
        database.deleteAllTests()
        view.updateRowSize(getRowSize())
        view.updateRecycle(database.getAllTests())
    }

    fun getRowSize(): Int {
        return database.getRowSize()
    }

    fun getAllTests(): List<TestRoom> {
        return database.getAllTests()
    }


}