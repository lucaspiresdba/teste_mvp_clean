package com.example.lucas.tstemvpclean.presenter.mainActivity

import com.example.lucas.data.repository.entity.Cell
import com.example.lucas.data.repository.entity.Fund
import com.example.lucas.data.repository.entity.TestRoom

interface MainActivityView {
    fun showItemsCells(item: Cell?)
    fun showItemsDownInfo(item: Fund?)
    fun error()
    fun updateRowSize(size: Int)
    fun updateRecycle(list:List<TestRoom>)
}