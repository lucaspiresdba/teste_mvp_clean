package com.example.lucas.tstemvpclean.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lucas.data.repository.entity.TestRoom
import com.example.lucas.tstemvpclean.R

class AdapterItensDB(var context: Context) : RecyclerView.Adapter<AdapterItensDB.AdapterViewHolder>() {

    private var itens: ArrayList<TestRoom> = ArrayList()

    fun clear() {
        itens.clear()
        notifyDataSetChanged()
    }

    fun addAll(list: List<TestRoom>) {
        list.forEach {
            itens.add(it)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.adapter, parent, false)
        return AdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return itens.size
    }

    override fun onBindViewHolder(holder: AdapterViewHolder, position: Int) {
        holder.bind(itens[position])
    }


    inner class AdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val name = itemView.findViewById<TextView>(R.id.nome)
        private val id1 = itemView.findViewById<TextView>(R.id.id1)
        private val id2 = itemView.findViewById<TextView>(R.id.id2)

        fun bind(item: TestRoom) {
            name.text = item.nome
            id1.text = item.tipo1.toString()
            id2.text = item.tipo2.toString()
        }
    }
}