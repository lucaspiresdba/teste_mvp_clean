package com.example.lucas.tstemvpclean.ui

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.lucas.data.repository.entity.Cell
import com.example.lucas.data.repository.entity.Fund
import com.example.lucas.data.repository.entity.TestRoom
import com.example.lucas.tstemvpclean.R
import com.example.lucas.tstemvpclean.presenter.mainActivity.MainActivityPresenter
import com.example.lucas.tstemvpclean.presenter.mainActivity.MainActivityView
import com.example.lucas.tstemvpclean.ui.adapter.AdapterItensDB
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainActivityView {

    @Inject
    lateinit var presenter: MainActivityPresenter

    lateinit var addButton: Button

    lateinit var deleteAllButton: Button

    lateinit var rowSize: TextView

    lateinit var recycle:RecyclerView

    private lateinit var adapter: AdapterItensDB


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AndroidInjection.inject(this)
        setupView()
        updateRowSize(presenter.getRowSize())
        updateRecycle(presenter.getAllTests())

        //chamada na api - resposta apenas aparece no log
        presenter.getCell()
        presenter.getFund()


        addButton.setOnClickListener {
            presenter.insertTest()
        }

        deleteAllButton.setOnClickListener {
            presenter.deleteAllTests()
        }
    }

    private fun setupView(){
        addButton = findViewById(R.id.add)
        deleteAllButton = findViewById(R.id.deleteall)
        rowSize = findViewById(R.id.row_size)
        recycle = findViewById(R.id.recycle)

        adapter = AdapterItensDB(this)
        recycle.adapter = adapter
        recycle.layoutManager = LinearLayoutManager(this)
    }

    override fun updateRecycle(list:List<TestRoom>) {
        adapter.clear()
        adapter.addAll(list)
    }

    override fun updateRowSize(size: Int) {
        rowSize.text = "itens no banco: $size"
    }

    override fun error() {

    }

    override fun showItemsCells(item: Cell?) {
        item?.cells?.forEach {
            Log.e("CELL_CALL", it?.message)
        }
    }

    override fun showItemsDownInfo(item: Fund?) {
        item?.screen?.info?.forEach {
            Log.e("FUND_CALL", it?.name)
        }
    }


}


