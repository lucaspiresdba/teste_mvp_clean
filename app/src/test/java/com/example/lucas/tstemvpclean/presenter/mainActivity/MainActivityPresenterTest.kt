package com.example.lucas.tstemvpclean.presenter.mainActivity


import com.example.lucas.data.repository.entity.Cell
import com.example.lucas.data.repository.entity.Fund
import com.example.lucas.data.repository.entity.TestRoom
import com.example.lucas.domain.dataBaseUseCase.DataBaseInterface
import com.example.lucas.domain.restUseCase.InfraInterface
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor

class MainActivityPresenterTest {
    private val immediateScheduler = object : Scheduler() {
        override fun createWorker(): Worker {
            return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
        }
    }

    @Mock
    lateinit var view: MainActivityView

    @Mock
    lateinit var infraInterface: InfraInterface

    @Mock
    lateinit var database: DataBaseInterface

    @InjectMocks
    lateinit var mainActivityPresenter: MainActivityPresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        RxJavaPlugins.setInitIoSchedulerHandler { immediateScheduler }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { immediateScheduler }
    }

    @Test
    fun `call apiCell list of cellitems`() {
        val responseCell = Cell()
        `when`(infraInterface.getCellsInfra()).thenReturn(Single.just(responseCell))
        mainActivityPresenter.getCell()
        verify(view, times(1)).showItemsCells(any())
        verify(view, never()).error()
    }

    @Test
    fun `call apiFund list of downinfo`() {
        val responseFund = Fund()
        `when`(infraInterface.getFundInfra()).thenReturn(Single.just(responseFund))
        mainActivityPresenter.getFund()
        verify(view, times(1)).showItemsDownInfo(any())
        verify(view, never()).error()
    }

    @Test
    fun `call apiFund list error`() {
        val error = Exception()
        `when`(infraInterface.getFundInfra()).thenReturn(Single.error<Fund>(error))
        mainActivityPresenter.getFund()
        verify(view, times(1)).error()
        verify(view, never()).showItemsDownInfo(any())
    }

    @Test
    fun `call apiCell list error`() {
        val error = Exception()
        `when`(infraInterface.getCellsInfra()).thenReturn(Single.error<Cell>(error))
        mainActivityPresenter.getCell()
        verify(view, times(1)).error()
        verify(view, never()).showItemsCells(any())
    }

    @After
    fun tearDown() {
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }

}