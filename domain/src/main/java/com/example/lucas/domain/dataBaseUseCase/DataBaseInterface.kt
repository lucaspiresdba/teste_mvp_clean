package com.example.lucas.domain.dataBaseUseCase

import com.example.lucas.data.repository.entity.TestRoom

interface DataBaseInterface {
    fun insertTest(test: TestRoom)
    fun deleteAllTests()
    fun getAllTests(): List<TestRoom>
    fun getRowSize(): Int
}