package com.example.lucas.domain.dagger.modules

import com.example.lucas.data.dagger.modules.TestRoomModule
import com.example.lucas.data.repository.dao.TestRoomDao
import com.example.lucas.domain.dataBaseUseCase.DataBaseInterface
import com.example.lucas.domain.dataBaseUseCase.DataBaseUseCase
import dagger.Module
import dagger.Provides

@Module(includes = [TestRoomModule::class])
class DataBaseUseCaseModule {
    @Provides
    fun providesDataBaseUseCase(testRoomDao: TestRoomDao): DataBaseInterface {
        return DataBaseUseCase(testRoomDao)
    }
}