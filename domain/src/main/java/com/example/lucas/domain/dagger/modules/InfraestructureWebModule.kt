package com.example.lucas.domain.dagger.modules

import com.example.lucas.data.apiservices.ApiCalls
import com.example.lucas.domain.restUseCase.InfraInterface
import com.example.lucas.domain.restUseCase.InfraestructureWeb
import dagger.Module
import dagger.Provides


@Module()
class InfraestructureWebModule {
    @Provides
    fun provides(webService: ApiCalls): InfraInterface {
        return InfraestructureWeb(webService)
    }
}