package com.example.lucas.domain.dataBaseUseCase

import com.example.lucas.data.repository.dao.TestRoomDao
import com.example.lucas.data.repository.entity.TestRoom

class DataBaseUseCase(private var testRoomDao: TestRoomDao) : DataBaseInterface {


    override fun insertTest(test: TestRoom) {
        testRoomDao.insert(test)
    }

    override fun deleteAllTests() {
        testRoomDao.deleteAll()
    }

    override fun getAllTests(): List<TestRoom> {
        return testRoomDao.getAll()
    }

    override fun getRowSize(): Int {
        return testRoomDao.getAll().size
    }
}