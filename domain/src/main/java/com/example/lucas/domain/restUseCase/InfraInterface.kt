package com.example.lucas.domain.restUseCase

import com.example.lucas.data.repository.entity.Cell
import com.example.lucas.data.repository.entity.Fund
import io.reactivex.Single

interface InfraInterface {

    fun getCellsInfra(): Single<Cell>
    fun getFundInfra(): Single<Fund>

}
