package com.example.lucas.domain.restUseCase

import com.example.lucas.data.apiservices.ApiCalls
import com.example.lucas.data.repository.entity.Cell
import com.example.lucas.data.repository.entity.Fund
import io.reactivex.Single

class InfraestructureWeb(private var webService: ApiCalls) : InfraInterface {

    override fun getCellsInfra(): Single<Cell> {
        return webService.getCells()
    }

    override fun getFundInfra(): Single<Fund> {
        return webService.getFund()
    }
}