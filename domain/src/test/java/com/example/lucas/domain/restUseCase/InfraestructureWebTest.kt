package com.example.lucas.domain.restUseCase

import com.example.lucas.data.apiservices.ApiCalls
import com.example.lucas.data.repository.entity.*
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.hamcrest.CoreMatchers.`is`
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.util.concurrent.Executor

class InfraestructureWebTest {
    private val immediateScheduler = object : Scheduler() {
        override fun createWorker(): Worker {
            return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
        }
    }

    @Mock
    lateinit var service: ApiCalls

    @InjectMocks
    lateinit var infraestructureWeb: InfraestructureWeb


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        RxJavaPlugins.setInitIoSchedulerHandler { immediateScheduler }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { immediateScheduler }
    }

    @Test
    fun `when call api return expected cell obj`() {
        val responseCell = Cell(listOf(CellsItem("um"), CellsItem("dois"), CellsItem("tres")))

        `when`(infraestructureWeb.getCellsInfra()).thenReturn(Single.just(responseCell))

        val cell = infraestructureWeb.getCellsInfra()
                .test()
                .assertValue(responseCell)
                .assertNoErrors()
                .assertComplete()
                .values()[0]

        assertThat(cell.cells!![0]!!.message, `is`("um"))
        assertThat(cell.cells!!.size, `is`(3))
    }

    @Test
    fun `when call api return expected fund obj`() {
        val responseFund = Fund(Screen(listOf(DownInfoItem("item1"), DownInfoItem("item2"), DownInfoItem("item3"))))

        `when`(infraestructureWeb.getFundInfra()).thenReturn(Single.just(responseFund))

        val fund = infraestructureWeb.getFundInfra()
                .test()
                .assertValue(responseFund)
                .assertNoErrors()
                .assertComplete()
                .assertValueCount(1)
                .values()[0]

        assertThat(fund.screen!!.downInfo!![2]!!.name, `is`("item3"))

    }

    @Test
    fun `when call api return expected cell error obj`() {
        val error = Exception("erro123")

        `when`(infraestructureWeb.getCellsInfra()).thenReturn(Single.error<Cell>(error))

        val result = infraestructureWeb.getCellsInfra()
                .test()
                .assertError(error)
                .assertNotComplete()
                .errors()[0]


        assertThat(error, `is`(result))
    }

    @Test
    fun `when call api return expected fund error obj`() {
        val error = Exception("banana")

        `when`(infraestructureWeb.getFundInfra()).thenReturn(Single.error<Fund>(error))

        val result = infraestructureWeb.getFundInfra()
                .test()
                .assertError(error)
                .assertNotComplete()
                .errors()[0]


        assertThat(error, `is`(result))


    }

    @After
    fun tearDown() {
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }
}